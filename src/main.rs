#![no_main]
#![no_std]

extern crate cortex_m_semihosting;
extern crate cortex_m;
extern crate cortex_m_rt;
extern crate embedded_hal;
extern crate panic_semihosting;

extern crate stm32f0;
#[macro_use]
extern crate stm32f042_hal as hal;
pub use stm32f0::stm32f0x2 as stm32;
mod nrf;
use nrf::Nrf24;
use stm32::Interrupt;
use hal::spi::Spi;
use hal::gpio::*;
use hal::prelude::*;
use hal::delay::Delay;
use embedded_hal::digital::{StatefulOutputPin};
use hal::spi::{Polarity, Phase, Mode};
use cortex_m::peripheral::Peripherals;
use cortex_m::interrupt::Mutex;
use cortex_m::asm::{wfi, bkpt};
use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;

struct FetOutputs {
    out1: gpioa::PA7<Output<PushPull>>,
    out2: gpiob::PB0<Output<PushPull>>,
    out4: gpiob::PB1<Output<PushPull>>,
    out3: gpioa::PA8<Output<PushPull>>
}

impl FetOutputs {
    pub fn new(out1: gpioa::PA7<Output<PushPull>>,
           out2: gpiob::PB0<Output<PushPull>>,
           out4: gpiob::PB1<Output<PushPull>>,
           out3: gpioa::PA8<Output<PushPull>>) -> Self {
        let mut fets = FetOutputs {
            out1: out1,
            out2: out2,
            out3: out3,
            out4: out4
        };
        fets.out1.set_low();
        fets.out2.set_low();
        fets.out3.set_low();
        fets.out4.set_low();
        fets
    }

    pub fn pin_change(&mut self, pin: u8, set: bool, toggle: bool) {
        match pin {
            0x01 => {
                if set || (toggle && self.out1.is_set_low()) {
                    self.out1.set_high();
                } else {
                    self.out1.set_low();
                }
            },
            0x02 => {
                if set || (toggle && self.out2.is_set_low()) {
                    self.out2.set_high();
                } else {
                    self.out2.set_low();
                }
            },
            0x04 => {
                if set || (toggle && self.out3.is_set_low()) {
                    self.out3.set_high();
                } else {
                    self.out3.set_low();
                }
            },
            _ => {
                if set || (toggle && self.out4.is_set_low()) {
                    self.out4.set_high();
                } else {
                    self.out4.set_low();
                }
            }
        }
    }

    pub fn command(&mut self, command: [u8; 8]) {
        let masks = command[2];
        let sets = command[3];
        let toggles = command[4];
        let mut mask: u8 = 0x01;
        while mask < 0x10 {
            if (masks & mask) != 0 {
                self.pin_change(mask, (sets & mask) == mask, (toggles & mask) == mask);
            }
            mask = mask << 1;
        }
    }

    pub fn display(&mut self) {
        hprintln!("Fet1: {}\nFet2: {}\nFet3: {}\nFet4: {}\n",
                  self.out1.is_set_high(),
                  self.out2.is_set_high(),
                  self.out3.is_set_high(),
                  self.out4.is_set_high()).unwrap();
    }
}


#[entry]
fn main() -> ! {
    pub const MODE: Mode = Mode {
        polarity: Polarity::IdleLow,
        phase: Phase::CaptureOnFirstTransition,
    };
    if let (Some(p), Some(cp)) = (stm32::Peripherals::take(), Peripherals::take()) {
        let mut rcc = p.RCC.constrain();

        let clocks = rcc.cfgr.sysclk(48.mhz()).freeze();
        let syscfg = p.SYSCFG_COMP;
        let exti = p.EXTI;

        let gpiob = p.GPIOB.split();
        let sck = gpiob.pb3.into_alternate_af0();
        let miso = gpiob.pb4.into_alternate_af0();
        let mosi = gpiob.pb5.into_alternate_af0();

        // Configure SPI with 100kHz rate
        let mut spi = Spi::spi1(p.SPI1, (sck, miso, mosi), MODE, 5_000_000.hz(), clocks);
        let gpioa = p.GPIOA.split();
        let mut rf_csn = gpioa.pa4.into_push_pull_output();
        let mut rf_ce = gpioa.pa2.into_push_pull_output();
        let mut nrf = Nrf24::new(spi, rf_csn, rf_ce, 0).unwrap();

        let button1 = gpioa.pa9.into_pull_up_input();
        let button2 = gpioa.pa10.into_pull_up_input();
        let button3 = gpioa.pa1.into_pull_up_input();
        let button4 = gpioa.pa6.into_pull_up_input();
        syscfg
                .syscfg_exticr1
                .modify(|_, w| unsafe { w.exti1().bits(1) });
        exti.imr.modify(|_, w| w.mr1().set_bit());
//        exti.imr.modify(|_, w| w.mr6().set_bit());
        exti.ftsr.modify(|_, w| w.tr1().set_bit());
//        exti.ftsr.modify(|_, w| w.tr6().set_bit());
        let mut nvic = cp.NVIC;
        nvic.enable(Interrupt::EXTI0_1);
                    unsafe { nvic.set_priority(Interrupt::EXTI0_1, 1) };
        nvic.clear_pending(Interrupt::EXTI0_1);

        let mut sw1 = gpioa.pa7.into_push_pull_output();
        let mut sw2 = gpiob.pb0.into_push_pull_output();
        let mut sw3 = gpiob.pb1.into_push_pull_output();
        let mut sw4 = gpioa.pa8.into_push_pull_output();
        let mut fets = FetOutputs::new(sw1, sw2, sw3, sw4);
        // turn on fet one.
        fets.command([0x20, 0, 1, 1, 0, 0, 0, 0]);

        /* (Re-)configure PA5 as output */
        let mut led = gpioa.pa5.into_push_pull_output();
        /* Get delay provider */
        let mut delay = Delay::new(cp.SYST, clocks);
        nrf.set_tx_address([0x05, 0x20, 0x20, 0x30, 0x01]).unwrap();
        nrf.transmit_data(&mut delay, [0x20, 0, 0x04, 0x04, 0x04, 0, 0, 0]).unwrap();
        /*
        match hprintln!("Status: 0x{:02X}", nrf.status().unwrap()) {
            Ok(_) => {},
            Err(_) => {}
        }
        */
//        fets.display();
        let mut runs = 0;
        loop {
            led.set_low();
            if runs == 11 {
                wfi();
                runs = 0;
            } else {
                delay.delay_ms(250_u8);
                delay.delay_ms(250_u8);
                delay.delay_ms(250_u8);
                delay.delay_ms(250_u8);
            }
            led.set_high();
            delay.delay_ms(5_u16);
            runs += 1;
        }
    }

    loop {
        continue;
    }
}

interrupt!(EXTI0_1, button_press);

fn button_press() {
//    bkpt();
}
