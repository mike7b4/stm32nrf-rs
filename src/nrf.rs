use hal::delay::Delay;
use embedded_hal::blocking;
use hal::spi::Spi;
use embedded_hal::digital::{OutputPin};
use hal::prelude::*;
#[derive(Debug)]
pub enum Error<E> {
    ReadError,
    WriteError,
    InvalidArgument,
    Spi(E)
}
pub struct Nrf24<SPI, CSN, CE> {
    address: u8,
    spi: SPI,
    csn: CSN,
    ce: CE,
    config: u8,
    status: u8
}

const NOP: u8 = 0xFF;
const FLUSH_TX: u8 = 0xE1;
const FLUSH_RX: u8 = 0xE2;
const REG_CONFIG: u8 = 0x00;
const REG_EN_AA: u8 = 0x01;
const REG_EN_RX_ADDR: u8 = 0x02;
const REG_SETUP_AW: u8 = 0x03;
const REG_SETUP_RETR: u8 = 0x04;
const REG_RF_CH: u8 = 0x05;
const REG_RF_SETUP: u8 = 0x06;
const REG_STATUS: u8 = 0x07;
const REG_RX_ADDR_P0: u8 = 0x0A;
const REG_RX_ADDR_P1: u8 = 0x0B;
const REG_RX_ADDR_P2: u8 = 0x0C;
const REG_RX_ADDR_P3: u8 = 0x0D;
const REG_RX_ADDR_P4: u8 = 0x0E;
const REG_RX_ADDR_P5: u8 = 0x0F;

const REG_TX_ADDR: u8 = 0x10;
const REG_RX_PW_P0: u8 = 0x11;
const REG_RX_PW_P1: u8 = 0x12;
const REG_RX_PW_P2: u8 = 0x13;
const REG_RX_PW_P3: u8 = 0x14;
const REG_RX_PW_P4: u8 = 0x15;
const REG_RX_PW_P5: u8 = 0x16;

const REG_FIFO_STATUS: u8 = 0x17;
const REG_WRITE: u8 = 0x20;

const CONFIG_MASK_RX_DR: u8 = 0x40;
const CONFIG_MASK_TX_DS: u8 = 0x20;
const CONFIG_MASK_MAX_RT: u8 = 0x10;
const CONFIG_EN_CRC: u8 = 0x08;
const CONFIG_CRC0: u8 = 0x04;
const CONFIG_PWR_UP: u8 = 0x02;
const CONFIG_PRIM_RX: u8 = 0x01;

const R_RX_PAYLOAD: u8 = 0x61;
const W_TX_PAYLOAD: u8 = 0xA0;
impl<E> From<E> for Error<E> {
    fn from(e: E) -> Self {
        Error::Spi(e)
    }
}
impl<E, SPI, CSN, CE> Nrf24<SPI, CSN, CE>
where
    SPI: blocking::spi::Transfer<u8, Error = E>,
    CSN: OutputPin,
    CE: OutputPin
{
   pub fn new(spi: SPI, rf_csn: CSN, ce: CE, address: u8) -> Result<Self, E> {
        let mut nrf = Nrf24 {
            address: address,
            spi: spi,
            csn: rf_csn,
            ce: ce,
            status: 0,
            config: 0
        };
        nrf.ce.set_low();
        nrf.csn.set_high();
        nrf.power_down()?;
        Ok(nrf)
    }

    fn set_register(&mut self, reg: u8, value: u8) -> Result<(), E> {
        self.reg_transfer(REG_WRITE | reg, value)?;
        Ok(())
    }

    fn register(&mut self, reg: u8) -> Result<u8, E> {
        self.reg_transfer(reg, 0xFF)
    }

    fn reg_transfer(&mut self, reg: u8, value: u8) -> Result<u8, E> {
        let mut dat: [u8; 2] = [reg, value];
        self.csn.set_low();
        let result = {
            let data = self.spi.transfer(&mut dat)?;
            Ok(data[1])
        };
        self.csn.set_high();
        result
    }

    pub fn flush_rx(&mut self) -> Result<(), E> {
        self.csn.set_low();
        let result = {
            self.spi.transfer(&mut [FLUSH_RX])?;
            Ok(())
        };
        self.csn.set_high();
        result
    }

    pub fn flush_tx(&mut self) -> Result<(), E> {
        self.csn.set_low();
        let result = {
            self.spi.transfer(&mut [FLUSH_TX])?;
            Ok(())
        };
        self.csn.set_high();
        result
    }

    pub fn transmit_data(&mut self, delay: &mut Delay, send_data: [u8; 8]) -> Result<(), E> {
        let mut send_data = send_data;
        self.enable_tx()?;
        self.csn.set_low();
        let result = {
            self.spi.transfer(&mut [W_TX_PAYLOAD])?;
            self.spi.transfer(&mut send_data)?;
            Ok(())
        };
        self.csn.set_high();
        if result.is_ok() {
            // lets send the data over the air
            self.ce.set_high();
            // TODO use ISR here instead of ugly delay
            delay.delay_ms(20_u8);
            self.ce.set_low();
        }

        result
    }

    pub fn nrf_fetch_rx_package(&mut self) -> Result<[u8; 8], E> {
        let mut incoming: [u8; 8] = [0xFF; 8];
        self.csn.set_low();
        let result = {
            self.spi.transfer(&mut [R_RX_PAYLOAD])?;
            self.spi.transfer(&mut incoming)?;
            Ok(incoming)
        };
        self.csn.set_high();
        result
   }

    pub fn set_tx_address(&mut self, address: [u8; 5]) -> Result<(), E> {
        let mut address = address;
        self.csn.set_low();
        let ret = {
            self.spi.transfer(&mut [REG_TX_ADDR | REG_WRITE])?;
            self.spi.transfer(&mut address)?;
            Ok(())
        };
        self.csn.set_high();
        ret
    }

    pub fn status(&mut self) -> Result<u8, E> {
        self.status = self.register(REG_STATUS)?;
        Ok(self.status)
    }

    pub fn set_channel(&mut self, channel: u8) -> Result<(), E> {
        self.set_register(REG_RF_CH, channel)
    }

    pub fn set_power_dbm(&mut self, pwr: u8) -> Result<(), E> {
        let mut value = self.register(REG_RF_SETUP)?;
        value &= !0x06;
        value |= pwr & 0x06;
        self.set_register(REG_RF_SETUP, value)
    }

    pub fn set_datarate(&mut self, rate: u8) -> Result<(), E> {
        let mut value = self.register(REG_RF_SETUP)?;
        value &= !0x28;
        value |= rate & 0x28;
        self.set_register(REG_RF_SETUP, value)
    }

    pub fn set_rx_address(&mut self, pipe: u8, address: [u8; 5], enable_autoack: bool) -> Result<(), Error<E>> {
        let mut address = address;
        if pipe > 5 {
            return Err(Error::InvalidArgument);
        }

        let mut data: [u8; 6] = [0; 6];
        self.csn.set_low();
        let ret = {
            self.spi.transfer(&mut [REG_RX_ADDR_P0+pipe | REG_WRITE])?;
            self.spi.transfer(&mut address)?;

            self.set_register(REG_RX_PW_P0+pipe, 8)?;
            let mut value = self.register(REG_EN_AA)?;
            if enable_autoack && (value & (1 << pipe)) == 0 {
                value |= 1 << pipe;
                self.set_register(REG_EN_AA, value)?;
            } else if !enable_autoack && (value & (1 << pipe)) != 0  {
                value &= !(1 << pipe);
                self.set_register(REG_EN_AA, value)?;
            }
            Ok(())
        };
        self.csn.set_high();
        ret
    }

    pub fn power_down(&mut self) -> Result<(), E> {
        // Config
        self.set_register(REG_CONFIG, 0x70)?;
        self.config = self.register(REG_CONFIG)?;

        // Status
        self.status = self.register(REG_STATUS)?;
        if (self.status & 0x70) != 0x00 {
            self.set_register(REG_STATUS, 0x70)?;
        }
        // Flush TX/RX
        self.flush_tx()?;
        self.flush_rx()?;
        self.status = self.register(REG_STATUS)?;
        // hprintln!("Status: 0x{:02X}\nConfig: 0x{:02X}", self.status, self.config) { Ok(_) => {}, Err(_) => {} };
        Ok(())
    }

    pub fn enable_tx(&mut self) -> Result<(), E> {
        self.set_register(REG_CONFIG, CONFIG_MASK_RX_DR | CONFIG_EN_CRC | CONFIG_PWR_UP)?;
        self.config = self.register(REG_CONFIG)?;
        Ok(())
    }
}

